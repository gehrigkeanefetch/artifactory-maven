# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.3

- patch: Incremented version of JFrog CLI.

## 0.3.2

- patch: Adding git in docker image

## 0.3.1

- patch: JFrog CLI version to 1.24.1

## 0.3.0

- minor: checking status of each executed command.

## 0.2.10

- patch: updated readme.

## 0.2.9

- patch: Added build-add-git (bag) command 

## 0.2.8

- patch: Updated links

## 0.2.7

- patch: Updated support link

## 0.2.6

- patch: Minor README improvements.
- patch: README cleanup and standardising on pipes variable validation messages.
- patch: Standardise pipe variable validation messages.

## 0.2.5

- patch: Updated readme

## 0.2.4

- patch: Updated readme

## 0.2.3

- patch: Updated readme

## 0.2.2

- patch: Build URL

## 0.2.1

- patch: Enable Debug for JFrog CLI

## 0.2.0

- minor: Minor Release

## 0.1.0

- minor: Minor Release

## 0.0.1

- patch: Initial release

