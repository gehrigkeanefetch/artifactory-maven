FROM maven:3.3.9-jdk-8-alpine

ENV VERSION=1.46.1

RUN apk add --update --no-cache jq bash curl wget maven git

RUN curl -fL https://getcli.jfrog.io $VERSION | sh

RUN chmod +x jfrog && mv jfrog /usr/bin/

COPY pipe /usr/bin/

ENTRYPOINT ["/usr/bin/pipe.sh"]
