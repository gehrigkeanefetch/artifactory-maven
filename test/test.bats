#!/usr/bin/env bats
#
# Test Artifactory Maven
#

set -e

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/artifactory-maven"}
  docker build -t ${DOCKER_IMAGE} .
}

@test "Publish Maven package to Artifactory" {

  # execute tests
  run docker run \
    -e ARTIFACTORY_URL=$ARTIFACTORY_URL \
    -e ARTIFACTORY_USER=$ARTIFACTORY_USER \
    -e ARTIFACTORY_PASSWORD=$ARTIFACTORY_PASSWORD \
    -e MAVEN_SNAPSHOT_REPO=$MAVEN_SNAPSHOT_REPO \
    -e MAVEN_RELEASE_REPO=$MAVEN_RELEASE_REPO \
    -e BUILD_NAME=$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH \
    -e BITBUCKET_BUILD_NUMBER=$BITBUCKET_BUILD_NUMBER \
    -e FOLDER="test" \
    -e DEBUG="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
  ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "0" ]]

}

@test "Publish Maven package to Artifactory Advance" {

  # execute tests
  run docker run \
    -e ARTIFACTORY_URL=$ARTIFACTORY_URL \
    -e ARTIFACTORY_USER=$ARTIFACTORY_USER \
    -e ARTIFACTORY_PASSWORD=$ARTIFACTORY_PASSWORD \
    -e MAVEN_SNAPSHOT_REPO=$MAVEN_SNAPSHOT_REPO \
    -e MAVEN_RELEASE_REPO=$MAVEN_RELEASE_REPO \
    -e MAVEN_COMMAND="clean test install" \
    -e BUILD_NAME=$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH \
    -e BITBUCKET_BUILD_NUMBER=$BITBUCKET_BUILD_NUMBER \
    -e FOLDER="test" \
    -e DEBUG="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd) \
  ${DOCKER_IMAGE}

  echo ${output}

  [[ "${status}" == "0" ]]

}
