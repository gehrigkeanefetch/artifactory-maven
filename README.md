# Bitbucket Pipelines Pipe: Artifactory Maven

This pipe triggers a Maven build, while resolving Maven dependencies and deploying Maven packages from and to [JFrog Artifactory](https://jfrog.com/artifactory/?utm_source=BitbucketPipes&utm_medium=UX).

By default, this pipe will also capture build-info and publish it to Artifactory as metadata associated with the built artifacts.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: JfrogDev/artifactory-maven:0.3.3
  variables:
    ARTIFACTORY_URL: '<string>'
    ARTIFACTORY_USER: '<string>'
    ARTIFACTORY_PASSWORD: '<string>'
    MAVEN_SNAPSHOT_REPO: '<string>'
    MAVEN_RELEASE_REPO: '<string>'
    # MAVEN_COMMAND: '<string>'
    # BUILD_NAME: '<string>' # Optional.
    # JFROG_CLI_TEMP_DIR: '<string>' # Optional.
    # JFROG_CLI_HOME_DIR: '<string>' # Optional.
    # COLLECT_ENV: '<boolean>' # Optional.
    # COLLECT_GIT_INFO: '<boolean>' # Optional.
    # COLLECT_BUILD_INFO: '<boolean>' # Optional.
    # FOLDER: '<string>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # EXTRA_BAG_ARGS: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable        | Usage                                     |
| --------------  | ----------------------------------------- |
| ARTIFACTORY_URL (\*)  | The JFrog Artifactory URL . |
| ARTIFACTORY_USER (\*)  | Artifactory User with permission to create and access artifacts.   |
| ARTIFACTORY_PASSWORD (\*)  | Password for Artifactory User. |
| MAVEN_SNAPSHOT_REPO (\*)  | Artifactory Maven repository that will be used to resolve snapshots. |
| MAVEN_RELEASE_REPO (\*)  | Artifactory Maven repository that will be used to resolve releases. |
| MAVEN_COMMAND (\*)  | Maven command to execute. default `clean install` |
| BUILD_NAME | Build Name. Default: `$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH` |
| JFROG_CLI_TEMP_DIR  | Specifies the JFrog CLI temp directory. Default: `.` |
| JFROG_CLI_HOME_DIR  | Specifies the JFrog CLI home directory.Default: `.` |
| COLLECT_ENV   | This flag is used to collect environment variables and attach them to a build. Default: `true` |
| COLLECT_GIT_INFO  | This flag is used to [collects the Git](https://www.jfrog.com/confluence/display/CLI/CLI+for+JFrog+Artifactory#CLIforJFrogArtifactory-BuildIntegration-AddingGitInformation) revision and URL from the local .git directory and adds it to the build-info. Default: `true` |
| COLLECT_BUILD_INFO  | This flag is used to publish build info to Artifactory. Default: `true` |
| FOLDER          | A folder containing the `package.json` file. Default to the current directory: `.` |
| EXTRA_ARGS      | Extra arguments to be passed to the JFrog CLI command (see [JFrog CLI docs](https://www.jfrog.com/confluence/display/CLI/CLI+for+JFrog+Artifactory) for more details). Defaults to unset. |
| EXTRA_BAG_ARGS  | Extra arguments to be passed to the build-add-git (bag) JFrog CLI command (see [JFrog CLI docs](https://www.jfrog.com/confluence/display/CLI/CLI+for+JFrog+Artifactory#CLIforJFrogArtifactory-BuildIntegration-AddingGitInformation) for more details). Defaults to unset. |
| DEBUG           | Set to _true_ to output additional debug information. Default: `false`. |

_(\*) = required variable._

## Details
This pipe allows you to configure Maven build tool to resolve dependencies and deploy build artifacts from and to [Artifactory](https://jfrog.com/artifactory/), while collecting build-info and storing it in Artifactory. 

## Prerequisites
[JFrog Artifactory](https://jfrog.com/artifactory/?utm_source=BitbucketPipes&utm_medium=UX) details are necessary to use this pipe.

- Add the Credentials as a [secured environment variable](https://confluence.atlassian.com/x/0CVbLw#Environmentvariables-Securedvariables) in Bitbucket Pipelines.
- [Artifactory Maven Repository](https://www.jfrog.com/confluence/display/RTF/Maven+Repository) is required.

## Examples

### Basic example 

Publishes Maven packages to [Artifactory Maven Repository](https://www.jfrog.com/confluence/display/RTF/Maven+Repository).

```yaml
script:
  - pipe: JfrogDev/artifactory-maven:0.3.3
    variables:
      ARTIFACTORY_URL: '<string>'
      ARTIFACTORY_USER: ${ARTIFACTORY_USER}
      ARTIFACTORY_PASSWORD: ${ARTIFACTORY_PASSWORD}
      MAVEN_SNAPSHOT_REPO: 'libs-snapshot'
      MAVEN_RELEASE_REPO: 'libs-release'
```

### Advanced example 
* Publishes Maven packages from the `app` directory to [Artifactory Maven Repository](https://www.jfrog.com/confluence/display/RTF/Maven+Repository). 

**Note**: We have `pom.xml` in `app` directory.

```yaml
script:
  - pipe: JfrogDev/artifactory-maven:0.3.3
    variables:
      ARTIFACTORY_URL: '<string>'
      ARTIFACTORY_USER: ${ARTIFACTORY_USER}
      ARTIFACTORY_PASSWORD: ${ARTIFACTORY_PASSWORD}
      MAVEN_SNAPSHOT_REPO: 'libs-snapshot'
      MAVEN_RELEASE_REPO: 'libs-release'
      MAVEN_COMMAND: 'clean test install'
      FOLDER: 'app'
```

* Complete example of building, publishing, scanning and promoting Maven packages to Artifactory using JFrog Pipes.

```yaml
    - step:
        services:
        - docker
        name: "maven-pipe-example"
        script:
        # Build and publish Maven packages to Artifactory
        - pipe: JfrogDev/artifactory-maven:0.3.3
          variables:
            ARTIFACTORY_URL: $ARTIFACTORY_URL
            ARTIFACTORY_USER: $ARTIFACTORY_USER
            ARTIFACTORY_PASSWORD: $ARTIFACTORY_PASSWORD
            MAVEN_SNAPSHOT_REPO: "libs-snapshot"
            MAVEN_RELEASE_REPO: "libs-release"
            BUILD_NAME: "maven-pipe-example"
        # Scan published build through Xray
        - pipe: JfrogDev/artifactory-xray-scan:0.2.7
          variables:
            ARTIFACTORY_URL: $ARTIFACTORY_URL
            ARTIFACTORY_USER: $ARTIFACTORY_USER
            ARTIFACTORY_PASSWORD: $ARTIFACTORY_PASSWORD
            BUILD_NAME: "maven-pipe-example"
        # Promote Build in Artifactory
        - pipe: JfrogDev/artifactory-build-promotion:0.3.3
          variables:
            ARTIFACTORY_URL: $ARTIFACTORY_URL
            ARTIFACTORY_USER: $ARTIFACTORY_USER
            ARTIFACTORY_PASSWORD: $ARTIFACTORY_PASSWORD
            TARGET_REPO: "maven-stage-local"
            STATUS: "Staged"
            BUILD_NAME: "maven-pipe-example"
        # Promote Build in Artifactory
        - pipe: JfrogDev/artifactory-build-promotion:0.3.3
          variables:
            ARTIFACTORY_URL: $ARTIFACTORY_URL
            ARTIFACTORY_USER: $ARTIFACTORY_USER
            ARTIFACTORY_PASSWORD: $ARTIFACTORY_PASSWORD
            TARGET_REPO: "maven-prod-local"
            STATUS: "Promoted"
            BUILD_NAME: "maven-pipe-example"
```

## Support
If you'd like help with this pipe, or you have an issue or feature request, [let us know on](https://bitbucket.org/JfrogDev/artifactory-maven/).

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
